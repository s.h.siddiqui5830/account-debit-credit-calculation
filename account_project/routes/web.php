<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BlanceController;
use App\Http\Controllers\ExpanceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//blance part-------------------------------------------------------------------------
Route::get('/blance', [BlanceController::class, 'FormBlance'])->name('blance');
Route::post('/blance/add_blance', [BlanceController::class, 'FormBlanceProcess'])->name('blance.insert');
Route::get('/all_blance', [BlanceController::class, 'AllBlance'])->name('all_blance');
//Route::get('/all_blance', [BlanceController::class, 'AllBlance'])->name('all_blance');

//expance part-------------------------------------------------------------------------
Route::get('/expance', [ExpanceController::class, 'FormExpance'])->name('expance');
Route::post('/expance/add_expance', [ExpanceController::class, 'FormExpanceProcess'])->name('expance.insert');
Route::get('/all_expance', [ExpanceController::class, 'AllExpance'])->name('all_expance');
