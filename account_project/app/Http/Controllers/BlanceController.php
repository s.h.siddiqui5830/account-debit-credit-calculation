<?php

namespace App\Http\Controllers;

use App\Models\Blance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlanceController extends Controller
{
    public function FormBlance()
    {
        
        return view('blance');

    }

    //blance form process------------------------------------------
    public function FormBlanceProcess(Request $request)
    {
        
        $request->validate(["opening_blance" => "required", "old_balance" => "nullable"]);
        
        $openingblance = $request->opening_blance;
        //$new_balance = $request->new_balance;
        

        $blance = new Blance();

        $op_blance = DB::table('blances')->latest()->first('opening_blance');

        $cal= $openingblance + $op_blance->opening_blance;

        $blance->opening_blance = $cal;
        $blance->new_balance = $openingblance;
       // $brand->status=1;
        $blance->save();

        //$request->session()->flash('message', 'Brand Inserted');

        return redirect()->route('all_blance');
    }

     public function AllBlance()
    {
        
    $blance = Blance::all();
    return view('all_blance', ['blance'=> $blance]);
    }
}
