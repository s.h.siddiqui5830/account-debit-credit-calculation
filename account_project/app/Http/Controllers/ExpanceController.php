<?php

namespace App\Http\Controllers;

use App\Models\Expance;
use App\Models\Blance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExpanceController extends Controller
{
   public function FormExpance()
    {
        
        return view('expance');

    }

    //expance form process------------------------------------------
    public function FormExpanceProcess(Request $request)
    {
        
        $request->validate(["expance_name" => "required", "expance_amount" => "required"]);
        
        $expancename = $request->expance_name;
        $expanceamount = $request->expance_amount;
        
        $blance = new Blance();
        $expance = new Expance();
        
        $op_blance = DB::table('blances')->latest()->first('opening_blance');

        $cal = $op_blance->opening_blance - $expanceamount;

        $blance->opening_blance = $cal;
        $blance->save();
        
        $expance->expance_name = $expancename;
        $expance->expance_amount = $expanceamount;
       // $brand->status=1;
        $expance->save();

        //$request->session()->flash('message', 'Brand Inserted');

        return redirect()->route('all_expance');
    }
    public function AllExpance()
    {
        
    $expance = Expance::all();
    return view('all_expance', ['expance'=> $expance]);
    }
}
